const button = document.getElementById("submitbutton");
const input = document.getElementById("input");
const answer = document.getElementById("answerText");
const conch = document.getElementById("conchText");

const options = ["Yes", "No", "Nothing", "::In a sarcastic voice:: Noooo", "Most likely", "Outlook good", "Can't tell you that",  "I don't know", "Theoretically? Sure.  Realistically? Probably not.", "Very doubtful", "Eat some food"]

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
  }

button.addEventListener("click", function() {
    if (input.value.length < 3){
        alert("The Conch has deemed your question to be stupid.  Ask another one.");
    } else{
        conch.innerText = "";
        let num = getRandomIntInclusive(0, options.length)
        answer.innerText = options[num];
    }
});
